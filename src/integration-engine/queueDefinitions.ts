import { config } from "@golemio/core/dist/integration-engine/config";
import { IQueueDefinition } from "@golemio/core/dist/integration-engine/queueprocessors";
import { Parkomats } from "#sch/index";
import { ParkomatsWorker } from "#ie/ParkomatsWorker";

const queueDefinitions: IQueueDefinition[] = [
    {
        name: Parkomats.name,
        queuePrefix: config.RABBIT_EXCHANGE_NAME + "." + Parkomats.name.toLowerCase(),
        queues: [
            {
                name: "refreshDataInDB",
                options: {
                    deadLetterExchange: config.RABBIT_EXCHANGE_NAME,
                    deadLetterRoutingKey: "dead",
                    messageTtl: 2 * 60 * 1000, // 2 minutes
                },
                worker: ParkomatsWorker,
                workerMethod: "refreshDataInDB",
            },
        ],
    },
];

export { queueDefinitions };
