import { SchemaDefinition } from "@golemio/core/dist/shared/mongoose";
import Sequelize from "@golemio/core/dist/shared/sequelize";

// MSO = Mongoose SchemaObject
// SDMA = Sequelize DefineModelAttributes

// data types and validations are guessed from sample data
const datasourceMSO: SchemaDefinition = {
    Channel: { type: String },
    DateFrom: { type: String },
    DateTime: { type: String },
    DateTo: { type: String },
    Id: { type: String, required: true },
    Price: { type: Number, required: true },
    Section: { type: String },
};

const outputSDMA: Sequelize.ModelAttributes<any> = {
    channel: Sequelize.STRING,
    parking_zone: Sequelize.STRING,
    price: Sequelize.INTEGER,
    ticket_bought: Sequelize.DATE,
    transaction_id: {
        primaryKey: true,
        type: Sequelize.STRING,
    },
    validity_from: Sequelize.DATE,
    validity_to: Sequelize.DATE,
};

// only for Validator
const outputMSO: SchemaDefinition = {
    channel: { type: String },
    parking_zone: { type: String },
    price: { type: Number, required: true },
    ticket_bought: { type: String },
    transaction_id: { type: String, required: true },
    validity_from: { type: String },
    validity_to: { type: String },
};

const forExport = {
    datasourceMongooseSchemaObject: datasourceMSO,
    name: "Parkomats",
    outputMongooseSchemaObject: outputMSO,
    outputSequelizeAttributes: outputSDMA,
    pgTableName: "parkomats",
};

export { forExport as Parkomats };
