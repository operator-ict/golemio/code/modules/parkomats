import sinon, { SinonSandbox, SinonSpy } from "sinon";
import { PostgresConnector } from "@golemio/core/dist/integration-engine/connectors";
import { ParkomatsWorker } from "#ie/ParkomatsWorker";

describe("ParkomatsWorker", () => {
    let worker: ParkomatsWorker;
    let sandbox: SinonSandbox;
    let testData: number[];
    let testTransformedData: number[];

    beforeEach(() => {
        sandbox = sinon.createSandbox({ useFakeTimers: true });

        sandbox.stub(PostgresConnector, "getConnection").callsFake(() =>
            Object.assign({
                define: sandbox.stub(),
            })
        );

        testData = [1, 2];
        testTransformedData = [1, 2];

        worker = new ParkomatsWorker();

        sandbox.stub(worker["dataSource"], "getAll").callsFake(() => Promise.resolve(testData));
        sandbox.stub(worker["transformation"], "transform").callsFake(() => Promise.resolve(testTransformedData));
        sandbox.stub(worker["model"], "save");
    });

    afterEach(() => {
        sandbox.restore();
    });

    it("should calls the correct methods by refreshDataInDB method", async () => {
        await worker.refreshDataInDB({});
        sandbox.assert.calledOnce(worker["dataSource"].getAll as SinonSpy);
        sandbox.assert.calledOnce(worker["transformation"].transform as SinonSpy);
        sandbox.assert.calledWith(worker["transformation"].transform as SinonSpy, testData);
        sandbox.assert.calledOnce(worker["model"].save as SinonSpy);
        sandbox.assert.callOrder(
            worker["dataSource"].getAll as SinonSpy,
            worker["transformation"].transform as SinonSpy,
            worker["model"].save as SinonSpy
        );
    });
});
